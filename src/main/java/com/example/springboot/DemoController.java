package com.example.springboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DemoController
 * @Author Jumy
 * @Description //TODO
 * @Date 2018/9/30 15:01
 * @Version 1.0
 **/
@RestController
@RequestMapping("/")
public class DemoController {
    @GetMapping("/helloWorld")
    public String helloworld(){
       return "HelloWorld";
    }
}
